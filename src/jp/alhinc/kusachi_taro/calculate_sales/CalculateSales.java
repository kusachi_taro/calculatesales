package jp.alhinc.kusachi_taro.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CalculateSales {
	public static void main(String[] args){
		if(args.length != 1){
			System.out.println("予期せぬエラーが発生しました");
			return;
		}
		HashMap<String, String> branchMap = new HashMap<String, String>();
		List<File> salesList = new ArrayList<File>();
		HashMap<String, Long> salesMap = new HashMap<String, Long>();
		
		if(!inputSalesMap(args[0], "branch.lst", salesMap, branchMap)){
			return;
		}
		
		if(!inputSalesList(args[0], salesList)){
			return;
		}
		
		if(!inputTotalSales(salesList, salesMap, branchMap)){
			return;
		}
		
		if(!outputSales(args[0], "branch.out", salesMap, branchMap)){
			return;
		}
	}
	
	public static boolean inputSalesMap(String path, String fileName, HashMap<String, Long>salesMap, HashMap<String, String>nameMap){
		BufferedReader br = null;
		try{
			File file = new File(path, fileName);
			if(!file.exists()){
				System.out.println("支店定義ファイルが存在しません");
				return false;
			}

			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null){
				String[] branch = line.split(",");

				if(!branch[0].matches("[0-9]{3}")){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				if(branch.length != 2){
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return false;
				}

				nameMap.put(branch[0], branch[1]);
				salesMap.put(branch[0], (long) 0);
			}
			return true;
		}
		catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(br != null){
				try {
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}
	
	public static boolean inputSalesList(String path, List<File>salesList){
		File dir = new File(path);
		File[] list = dir.listFiles();

		for(int i=0; i < list.length; i++){
			if((list[i].getName()).matches("[0-9]{8}.rcd") && list[i].isFile()){
				salesList.add(list[i]);
			}
		}

		for(int i=0; i < (salesList.size())-1; i++){
			int fileName = Integer.parseInt(((salesList.get(i)).getName()).replaceAll(".rcd",""));
			int fileName2 = Integer.parseInt(((salesList.get(i+1)).getName()).replaceAll(".rcd",""));
			if(fileName2 - fileName != 1){
				System.out.println("売上ファイル名が連番になっていません");
				return false;
			}
		}
		return true;
	}
	
	public static boolean inputTotalSales(List<File>salesList, HashMap<String, Long>salesMap, HashMap<String, String>nameMap){
		BufferedReader br = null;
		try{
			for(int i=0; i < salesList.size(); i++){
				List<String> salesAmount = new ArrayList<String>();
				FileReader fr = new FileReader(salesList.get(i));
				br = new BufferedReader(fr);

				String line;
				while((line = br.readLine()) != null){
					salesAmount.add(line);
				}

				if(salesAmount.size() != 2){
					System.out.println(((salesList.get(i)).getName()) + "のフォーマットが不正です");
					return false;
				}

				if(!salesAmount.get(1).matches("[0-9]*")){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}

				if(!nameMap.containsKey(salesAmount.get(0))){
					System.out.println(((salesList.get(i)).getName()) + "の支店コードが不正です");
					return false;
				}

				long totalSales = Long.parseLong(salesAmount.get(1));
				totalSales = salesMap.get(salesAmount.get(0)) + Long.parseLong(salesAmount.get(1));

				if(totalSales >= 10000000000L){
					System.out.println("合計金額が10桁を超えました");
					return false;
				}

				salesMap.put(salesAmount.get(0), totalSales);
			}
			return true;
		}

		catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(br != null){
				try {
					br.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}
	public static boolean outputSales(String path, String fileName, HashMap<String, Long>salesMap, HashMap<String, String>nameMap){
		BufferedWriter bw = null;
		try {
			File dir = new File(path, fileName);
			FileWriter fw = new FileWriter(dir);
			bw = new BufferedWriter(fw);
	
			for(String key: salesMap.keySet()){
				String name = nameMap.get(key);
				Long value = salesMap.get(key);
				
				bw.write(key + "," + name + "," + value);
				bw.newLine();
			}
			return true;

		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return false;
		}
		finally {
			if(bw != null){
				try {
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
					return false;
				}
			}
		}
	}
}
